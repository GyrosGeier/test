pipeline {
	agent {
		label 'msvc'
	}
	environment {
		VCPKG_FEATURE_FLAGS = 'manifests'
	}
	stages {
		stage('Checkout vcpkg') {
			steps {
				checkout([$class: 'GitSCM', \
					branches: [[name: 'vcpkg/master']], \
					doGenerateSubmoduleConfigurations: false, \
					extensions: [ \
						[$class: 'RelativeTargetDirectory', relativeTargetDir: 'vcpkg'] ], \
					submoduleCfg: [], \
					userRemoteConfigs: [ \
						[name: 'vcpkg', url: 'https://github.com/microsoft/vcpkg'] ] ])
			}
		}
		stage('Checkout kicad') {
			steps {
				checkout([$class: 'GitSCM', \
					branches: [[name: 'kicad/master']], \
					doGenerateSubmoduleConfigurations: false, \
					extensions: [ \
						[$class: 'RelativeTargetDirectory', relativeTargetDir: 'src'] ], \
					submoduleCfg: [], \
					userRemoteConfigs: [ \
						[name: 'kicad', url: 'https://gitlab.com/kicad/code/kicad.git'] ] ])
			}
		}
		stage('Build vcpkg') {
			steps {
				dir('vcpkg') {
					bat 'call bootstrap-vcpkg.bat -disableMetrics'
				}
			}
		}
		stage('Clean') {
			steps {
				catchError {
					bat 'cmd /c rd /s /q build'
				}
			}
		}
		stage('Build dependencies') {
			steps {
				bat 'vcpkg\\vcpkg install'
			}
		}
		stage('Build kicad') {
			steps {
				cmakeBuild installation: 'InSearchPath', \
					sourceDir: 'src', \
					buildDir: 'build', \
					cmakeArgs: "-DCMAKE_TOOLCHAIN_FILE:PATH=${WORKSPACE}\\vcpkg\\scripts\\buildsystems\\vcpkg.cmake -DVCPKG_MANIFEST_INSTALL:BOOL=ON",
					steps: [ \
						[args: '-- /M', withCmake: true] ]
			}
		}
	}
}
